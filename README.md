Workflow
--------

Software Projects are rarely commenced with detailed [requirements](https://en.wikipedia.org/wiki/Requirement)

Often they being with the desire to scratch an 'itch' and with some statement of 
[intent](https://en.wikipedia.org/wiki/Intent_(military))

These intent's often lead to some [prototype](https://en.wikipedia.org/wiki/Prototype) 
in which the [features](https://en.wikipedia.org/wiki/Software_feature) are quickly evaluated.

There is a moment of recognition when the ability to have
[reproducible](https://en.wikipedia.org/wiki/Reproducibility) 
[behaviour](https://en.wikipedia.org/wiki/Behavior-driven_development).
At this point [tests](https://en.wikipedia.org/wiki/Test_method) tend to be added to this 
[environment](https://en.wikipedia.org/wiki/Environment_(systems)) to assist in the systems being 
[reproducible](https://en.wikipedia.org/wiki/Reproducibility)

The intention of this collection of files is to build a workflow which is frictionless 
for those involved in its consumption, while attempting to integrate the ability to account and audit
the work performed during the 
[developement](https://en.wikipedia.org/wiki/Software_development), 
[deployment](https://en.wikipedia.org/wiki/IT_infrastructure_deployment) and 
[instantation](https://en.wikipedia.org/wiki/Instance_(computer_science)) 

It is an attempt to reflect the values found in the trustable hypothesis found here
 https://gitlab.com/trustable/overview/wikis/hypothesis-for-software-to-be-trustable
 
This is an idea demonstrator. NOT a production implementation.
Written by Edmund J. Sutcliffe

To see the model executing you require only the ability to run BASH and git

- The model is more detailed in [model.md](https://gitlab.com/thoughtful-solutions/workflow/blob/master/model.md)
- The [intents](https://en.wikipedia.org/wiki/Intent_(military)) can me found in the 
    [intents/](https://gitlab.com/thoughtful-solutions/workflow/blob/master/intents) directory
- The [features](https://en.wikipedia.org/wiki/Software_feature) can be found in the 
    [features/](https://gitlab.com/thoughtful-solutions/workflow/blob/master/features) directory
- The [environments](https://en.wikipedia.org/wiki/Environment_(systems)) configuration can be found in the 
    [environments/](https://gitlab.com/thoughtful-solutions/workflow/blob/master/model.md) directory
- The implementation can be found in the directory 
    [src/](https://gitlab.com/thoughtful-solutions/workflow/blob/master/srv) directory
- Setup Instructions can be found in 
    [setup.md](https://gitlab.com/thoughtful-solutions/workflow/blob/master/setup.md)
- Examples can be found in [example.md](https://gitlab.com/thoughtful-solutions/workflow/blob/master/example.md)