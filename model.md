 Evidence should be generated as much as possible a side effect of normal usage
 Version Control is crucial to the understanding of change
 Git is the most reliable form of change control
 
 
- Changes are submitted to the Change Tracker (git)
- Evidence Store records what was done
- on change evidence Gate requests orchestrator create an environment
- on environment evidence Gate requests Constructor create artifacts
- on artifact evidence Gate requests Constructor create an environment
- on environment evidence Gate requests Validator create test report
- on test report evidence Gate requests Review
- on review report evidence Gate requests Merge

```
 commit --> Change Tracker --> Evidence
                               Evidence -> Gate -> orchestrator -> constructor -> artifact
                                        <------------------------- constructor <- change tracker
                                           Gate -> orchestrator -> validator <- artifact
                                        <------------------------- validator <- change tracker
                                           Gate -> Review <- Change Tracker
                                        --------->  validator evidence
                                        --------->  constructor evidence
                                           Gate -> Merge
```
```mermaid
sequenceDiagram
    participant Developer
    participant Change
    participant Evidence
    participant Gate
    participant Orchestrator
    participant Constructor
    participant Validator
    participant Artifact
    participant Review
   Developer->Change:commit
   Change->Evidence:t.change
   Evidence->Gate:t.change
   Gate->Orchestrator:t.env
   Orchestrator->Constructor:t.build
   Constructor->Artifact:t_artifact_list
   Gate->Orchestrator:t.env
   Orchestrator->Validator:t.artifact_list
   Validator->Evidence:t.test_result_list
   Evidence->Gate:t.artifact_list
   Gate->Review:t.test_result_list,t.artifact_list
```