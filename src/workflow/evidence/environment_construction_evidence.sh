#!/bin/bash
# <edmund.sutcliffe@codethink.com>
# Write Evidence: How do we encode Evidence
# Type->Value Mapping
#  CSE: (Change Summission Evidence)   CVE:(Change Validation Evidence)                CRE:(Change Review Evidence)              
#  change-submission-id -> git SHA      test-execution-id       -> $(date +%s)                 change-review-id ->  SHA  
#           change-list -> --name-only  validation-results-list -> [URI of Results]                     NULL FIELD
#                                       test-id                 -> list of tests       change-submission-id-list -> SHA List 
#                                       change-validation-type  -> Unit|Static|System     test-execution-id-list -> TBD
#                                        artifact-deployment-id -> SHA of Deployment                    NULL FIELD
#        contributor-id -> git Author                 tester-id -> git Author                        reviewer-id -> git Author
#             date-time -> git date                   date-time -> git date                            date-time -> git date
#               comment -> git comment                  comment -> git comment                           comment -> git comment
#
# ECE:(Evidence Construction Evidence)  ACE: (Artifact Construction Evidence)                ADE: (Artifact Deployment Evidence)
#  environment-construction-id ->                artifact-construction-id ->                        artifact-deploment-id ->
#                                                       artifact-store-id -> URI of artifact       artifact-store-id-list -> list of artifact-construction-id
#                                             environment-construction-id ->                  environment-construction-id ->
#                                                                build-id -> git SHA           
#              orchestrator-id -> git Author               constructor-id -> git Author                   orchestrator-id -> git Author                
#                    date-time -> git date                      date-time -> git date                           date-time -> git date                  
#                      comment -> git comment                     comment -> git comment                          comment -> git comment               
#

function read_evidence () {
           local evidence_type=$1
         for noteid in $(git notes list | cut -d' ' -f2)
         do
           cme=`git log -1 --pretty=format:'%ae:%at:%ce:%ct:%H' $noteid`
           for evidence in $(git notes show $noteid)
           do
             if [[ "$evidence" == $evidence_type:* ]] ;
             then 
               echo "[Policy] evidence [$evidence:$cme]"
             else
               echo "[Policy] unknown evidence [$evidence]"
             fi
           done 
         done
         return 0
}

function write_evidence() {
           local    evidence_type=$1
           local  evidence_author=$2
           local evidence_content=$3
           local evidence_comment=$4
# DBG echo $evidence_type $evidence_author $evidence_content $evidence_comment
           git commit --quiet --allow-empty --author="$evidence_author" -m "$evidence_comment"
           git notes append -m "$evidence_type:$evidence_content"
}

function write_CRE() {
   echo WRITE_CRE
   # Assume we are in the current branch
   # so from where we are to the begining of the branch
   #
   local span=`git rev-list master..`
   local i=0;
   local commit;
   local comment;
   local approved;
   local reviewer="$USER <$USER@FAKE-ORG.COM>";
   local evidence;

   # Start by iterating the commit list and collect information
   for commit in $span;
     do
      id=`git log -1 --pretty=format:'%ae:%ce:%H' $commit`
      cf=`git show --name-only  --oneline  --no-commit-id --pretty=format:'' $commit`
      commitid[$i]=`cut -d':' -f3 <<< $id `  
      author[$i]=`cut -d':' -f1 <<< $id `  
      commiter[$i]=`cut -d':' -f2 <<< $id `  
      filelist[$i]=$cf
      (( ++i ))  
     done

   if [ -t 0 ]; then
      echo Please Review
      # We are running this interactively
      for ((i = 0; i < ${#commitid[@]}; ++i)); 
        do
         echo "Author [${author[$i]}] did commit-id [${commitid[$i]}]"
         echo "which changed [${filelist[$i]}]"
        done  
      echo "Any Comments about these changed <EOF> to terminate"
      comment=""
      while read -r; do
        comment+=${REPLY}
        comment+='\n'
      done
      read -p "Are the Changes Approved ?[N|Y]"
      if [[ $REPLY =~ ^[Yy]$ ]]
      then 
         tmp="APPROVED\n" 
         tmp+=$comment
         comment=$tmp
      fi
   else
      # File or pipe Input
      comment="Approved automated review without human input"
   fi
  #Build the evidence
  evidence="change-review-id:$span:test-execution-id"
  write_evidence "CRE" "$reviewer" "$evidence" "$comment"

}

#  read_evidence CRE
#  write_evidence CRE "Edmund Sutcliffe <edmund@panic.fluff.org>" "nn:pp:ff:ll:LL:" "here is a comment about this"
   write_CRE
exit 

#        for field in $(echo $evidence | tr : \\n)
#          do
#           i=$((i+1))
#           echo " [$i] [$field]" 
#         done
