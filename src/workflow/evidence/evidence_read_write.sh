#!/bin/bash
# <edmund.sutcliffe@codethink.com>
#
#  Evidence is meta data which is available within the
#  change repository. Idealy this meta data does not 
#  require additional assistance from the developer
#  It should be machine parsable.
#
#  It is associated with the change repository so that it
#  updated as things change in the repo
#
function read_evidence () {
           local evidence_type=$1
         for noteid in $(git notes list | cut -d' ' -f2)
         do
           cme=`git log -1 --pretty=format:'%ae:%at:%ce:%ct:%H' $noteid`
           for evidence in $(git notes show $noteid)
           do
             if [[ "$evidence" == $evidence_type:* ]] ;
             then 
               echo "[Policy] evidence [$evidence:$cme]"
             else
               echo "[Policy] unknown evidence [$evidence]"
             fi
           done 
         done
         return 0
}

function write_evidence() {
           local    evidence_type=$1
           local  evidence_author=$2
           local evidence_content=$3
           local evidence_comment=$4
# DBG echo $evidence_type $evidence_author $evidence_content $evidence_comment
           git commit --quiet --allow-empty --author="$evidence_author" -m "$evidence_comment"
           git notes append -m "$evidence_type:$evidence_content"
}

