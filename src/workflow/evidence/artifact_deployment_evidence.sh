#!/bin/bash
# <edmund.sutcliffe@codethink.com>
# Write Evidence: How do we encode Evidence
# Type->Value Mapping
#   ADE: (Artifact Deployment Evidence)
#        artifact-deploment-id ->
#       artifact-store-id-list -> list of artifact-construction-id
#  environment-construction-id ->
#              orchestrator-id -> git Author                
#                    date-time -> git date                  
#                      comment -> git comment               
#
function write_ADE() {
echo WRITE_ADE
   # Assume we are in the current branch
   # so from where we are to the begining of the branch
   #
   local span=`git rev-list master..`
   local i=0;
   local commit;
   local comment;
   local approved;
   local reviewer="$USER <$USER@FAKE-ORG.COM>";
   local evidence;

   # Start by iterating the commit list and collect information
   for commit in $span;
     do
      id=`git log -1 --pretty=format:'%ae:%ce:%H' $commit`
      cf=`git show --name-only  --oneline  --no-commit-id --pretty=format:'' $commit`
      commitid[$i]=`cut -d':' -f3 <<< $id `  
      author[$i]=`cut -d':' -f1 <<< $id `  
      commiter[$i]=`cut -d':' -f2 <<< $id `  
      filelist[$i]=$cf
      (( ++i ))  
     done

   if [ -t 0 ]; then
      echo Please Review
      # We are running this interactively
      for ((i = 0; i < ${#commitid[@]}; ++i)); 
        do
         echo "Author [${author[$i]}] did commit-id [${commitid[$i]}]"
         echo "which changed [${filelist[$i]}]"
        done  
      echo "Any Comments about these changed <EOF> to terminate"
      comment=""
      while read -r; do
        comment+=${REPLY}
        comment+='\n'
      done
      read -p "Are the Changes Approved ?[N|Y]"
      if [[ $REPLY =~ ^[Yy]$ ]]
      then 
         tmp="APPROVED\n" 
         tmp+=$comment
         comment=$tmp
      fi
   else
      # File or pipe Input
      comment="Approved automated review without human input"
   fi
  #Build the evidence
  evidence="change-review-id:$span:test-execution-id"
  write_evidence "CRE" "$reviewer" "$evidence" "$comment"

}

#  read_evidence CRE
#  write_evidence CRE "Edmund Sutcliffe <edmund@panic.fluff.org>" "nn:pp:ff:ll:LL:" "here is a comment about this"
   write_CRE
exit 

#        for field in $(echo $evidence | tr : \\n)
#          do
#           i=$((i+1))
#           echo " [$i] [$field]" 
#         done
