Now lets demonstrate that we can use git
 now add a file and commit
      echo >f.md
      git add f.md
      git commit -m "some comment"
  now we push this to the centre
      git push origin master
      git push origin refs/notes/*
  So we now have a single file in the repo and no notes
  Now we want to do a review
       git commit --allow-empty -m "evidence-type"
          --author="Human Being <hbeing@org.com>"
       git notes append -m "review"
       git push origin master
       git push origin refs/notes/*:refs/notes/*
