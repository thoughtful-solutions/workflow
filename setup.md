Start by creating a bare repo

```
   mkdir repo
   cd repo
   git init --bare .
   cd hooks
   for i in `ls *.sample`
    do 
      ln -s hook `basename $i .sample`
    done;
   mkdir workflow
   
```

 Start by building your own clone of the repo

``` 
   cd ../..
   mkdir mine
   cd mine
   git clone ../repo/ .
```

We are making use of [git notes](https://git-scm.com/docs/git-notes) to store evidence
These are not duplicated automatically. So we now clone the remote git notes 

```
    git fetch origin refs/notes/*:refs/notes/*
```
Test is done making use of a 
[BDD](https://github.com/binkley/shell/blob/master/plain-bash-testing/README.md)
approach